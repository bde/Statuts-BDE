\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{document_officiel}[2024/03/22]
\LoadClass[a4paper, 11pt]{report}

% Gestion de l'encodage
\RequirePackage[utf8]{inputenc}
\RequirePackage[TS1,T1]{fontenc}

% Gestion du français
\RequirePackage[french]{babel}
\RequirePackage{anyfontsize}

% Gestion des marges
\RequirePackage[margin=3cm]{geometry}
\RequirePackage[indent, skip=12pt plus 2pt]{parskip}

% Gestion des titres
\RequirePackage{titlesec}
\RequirePackage{appendix}
\RequirePackage{etoolbox, apptools}

\setcounter{secnumdepth}{4}

\titleclass{\part}{straight}
\titleformat{\part}[hang]{\normalfont\huge\bfseries\center\uppercase}{}{0pt}{}
\titlespacing{\part}{0pt}{40pt}{40pt}

\titleclass{\chapter}{straight}
\titleformat{\chapter}[hang]{\normalfont\Large\bfseries\center\uppercase}{%
\IfAppendix{Annexe}{Article} \thechapter~:~}{0pt}{}
\titlespacing{\chapter}{0pt}{40pt}{40pt}

\titleformat{\section}[hang]{\normalfont\Large\bfseries}{%
Article \thesection~:~}{0pt}{}
\titlespacing{\section}{0pt}{20pt}{20pt}

\titleformat{\subsection}[hang]{\normalfont\large\bfseries}{%
Article \thesubsection~:~}{0pt}{}
\titlespacing{\subsection}{0pt}{10pt}{10pt}

\titleformat{\subsubsection}[hang]{\normalfont\bfseries}{%
Article \thesubsubsection~:~}{0pt}{}
\titlespacing{\subsubsection}{0pt}{10pt}{10pt}

% Gestion de la table des matières
\RequirePackage{titletoc}

% Gestion des en-têtes et des pieds de page
\RequirePackage{fancyhdr}

% Gestion des couleurs (pour les hyperliens)
\RequirePackage[dvipsnames]{xcolor}

% Ajout d'espaces intelligents
\RequirePackage{xspace}

% Pour surligner
\RequirePackage{soulutf8}

% Gestion des hyperliens
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{cleveref}
% Ne gère pas les “:“ dans les labels, c'est pas normal mais je ne sais pas ce
%qu'il se passe.
\hypersetup{colorlinks=true,citecolor=red}
\crefname{chapter}{Article}{Articles}
\crefname{section}{Article}{Articles}
\crefname{appendix}{Annexe}{Annexes}
\crefname{subsection}{Article}{Articles}
\crefname{subsubsection}{Article}{Articles}
\AtBeginEnvironment{appendices}{\crefalias{section}{appendix}}
\AtBeginEnvironment{appendices}{\appendixtrue}

% Gestion des listes
\RequirePackage{enumitem}

% Ajout du caractère euro (€)
\RequirePackage{eurosym}

\newcommand{\gui}[1]{\og{}#1\fg{}}
\newcommand{\md}[0]{\textperiodcentered}
\newcommand{\VSS}{\textsf{VSS}\xspace}
\newcommand{\orga}{l'Organisateur\xspace}
\newcommand{\red}[1]{{\sethlcolor{VioletRed}\hl{#1}}}
\newcommand{\green}[1]{{\sethlcolor{YellowGreen}\hl{#1}}}
\newcommand{\yellow}[1]{{\sethlcolor{Goldenrod}\hl{#1}}}

% Diff
\newcommand{\explaincomment}{%
	\begin{itemize}
		\item \red{En rouge les suppressions}
		\item \green{En vert les ajouts}
		\item \yellow{En jaune les commentaires}
	\end{itemize}
	\yellow{Pour des raisons de clarté, les corrections mineures (conjugaison, grammaire, écriture
	inclusive, etc) ne sont pas forcément mises en évidences.}}

% Première page
\makeatletter
\newcommand{\dateAG}[1]{\def\@dateAG{#1}}
\newcommand{\dateReu}[1]{\def\@dateReu{#1}}
\newcommand{\terminaison}[1]{\def\@terminaison{#1}}
\let\@dateAG\@empty
\let\@dateReu\@empty
\let\@terminaison\@empty

\renewcommand{\maketitle}{%
    \begin{titlepage}
        \vspace*{\fill}
        \noindent\rule{\textwidth}{0.4pt}
        \begin{center}
            {\LARGE
            Amicale des Élèves de l'École Normale Supérieure Paris-Saclay
            \par}
            \vspace*{3em}
            {\large
            \sc
            \@title
            \par}
            \vspace*{1.5em}
            {\large
            \ifx\@dateAG\@empty
                \vspace*{\baselineskip}
                \vspace*{1em}
            \else{
                Adopté\@terminaison~par l'assemblée générale du \@dateAG\par}
                % les statuts sont "adoptÉS" mais le RI est "adoptÉ" et une charte
                % est "adoptÉE"
            \fi
            \ifx\@dateReu\@empty
                \vspace*{\baselineskip}
                \vspace*{1em}
            \else{
                Modifié\@terminaison~par le bureau le \@dateReu\par}
                % Le RI est "modifiÉ" mais une charte est "modifiÉE"
            \fi}
        \end{center}\par
        \noindent\rule{\textwidth}{0.4pt}
        \vspace*{\fill}
    \end{titlepage}}

\newcommand{\makeend}{%
    \vspace*{\fill}

    \nopagebreak

    \noindent\rule{\textwidth}{0.4pt}
    \@onelevel@sanitize\@title
    \def\@statuts{Statuts}
    \@onelevel@sanitize\@statuts
    \ifx\@title\@statuts
        Les présents statuts annulent et remplacent toute disposition
        statutaire antérieure.
    \else
    \fi
    \nopagebreak
    \begin{flushright}
        Fait à Gif-sur-Yvette le
        \ifx\@dateReu\@empty\@dateAG\else{\@dateReu}\fi.
    \end{flushright}
    \pagebreak}

\makeatother
