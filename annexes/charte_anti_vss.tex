\documentclass{document_officiel}

\title{Charte anti-VSS}
\terminaison{e} % {Adopté,Modifié}{,e,s,es}
\dateAG{3 mars 2021}
\dateReu{1\ier février 2023}


\begin{document}

    \maketitle

    \part{PRÉAMBULE}

        Toutes les écoles du plateau de Saclay\footnote{Ici, le plateau de
        Saclay est considéré comme une zone géographique, non administrative.},
        plus simplement appelé par la suite \gui{plateau}, sont amenées à
        connaître des violences sexuelles et sexistes, ci-après dénommées \VSS,
        durant les différents évènements qu'elles organisent. Pourtant,
        les modalités de prévention et de gestion de celles-ci sont inégales.

        La présente charte propose en ce sens d’instaurer une base commune à
        toutes les écoles du plateau de gestion de ces \VSS.

        Celle-ci à été co-construite par une dizaine d’élèves des 8 écoles du
        plateau de Saclay à l'heure de sa rédaction, à savoir l'École
        polytechnique, l'École Normale Supérieure de Paris-Saclay,
        CentraleSupélec, Télécom Paris, l'École nationale de la statistique et
        de l'administration économique Paris, l'École nationale supérieure de
        techniques avancées, l'institut national des sciences et industries du
        vivant et de l'environnement\footnote{Appelé communément AgroParisTech}
        et l'École Supérieure d'Optique.

        Cette co-construction s’est faite pour démontrer le besoin de
        solidarité de toutes les écoles envers toutes les autres afin de
        traiter un sujet profond, complexe et quotidien.

        Elle s’articule en deux volets : ce qui est proposé à l'administration
        et ce qui est proposé aux associations étudiantes.

        Ce volet associatif présente les règles à mettre en place dans les
        associations. Il est donc proposé qu'après sa lecture, les associations
        signent la charte et l'introduisent dans leurs statuts de façon
        pérenne.

        Enfin, cette charte impose une base commune de mesures à prendre par
        les associations de chaque école, mais ne constitue pas une ligne
        directrice de protection contre les violences sexuelles et sexistes
        dans ces écoles.

        En tant qu'élève, vous pouvez ajouter du poids à cette charte en la
        signant : vous indiquez alors votre volonté que les associations des
        écoles du plateau respectent cette charte.

    \section*{Définitions et concepts utilisés dans cette charte}

        Un \textbf{responsable anti-\VSS} est une personne formée pour réagir
        aux situations de \VSS et venir en aide aux personnes ayant subi une
        \VSS.

        Une \textbf{\textit{safe-zone}} est un espace calme et bienveillant
        capable d'accueillir et de sécuriser toute personne le souhaitant,
        et qui a pour but d'informer, d'écouter et d'accompagner autour des
        questions d'agression, de harcèlement, de consentement, etc.

        La \textbf{culture du viol} est un concept sociologique utilisé pour
        qualifier un ensemble d'attitudes et de comportements partagés au sein
        d'une société donnée qui minimisent, normalisent, voire encouragent le
        viol. La culture du viol est vue de façon graduelle, allant de
        l'institutionnalisation du viol jusqu'à sa sanction.

    \part{ARTICLES}

    \chapter{Gestion d'un évènement festif}

        \section{Généralités}

            Avant l'évènement, la ou les associations organisatrices, ci-après
            dénommées \orga, s'engage à relayer un questionnaire de
            sensibilisation à toutes les personnes présentes.

            Après l'évènement, \orga s'engage à relayer le questionnaire de
            recensement des \VSS à toutes les personnes présentes à la soirée
            \footnote{Le lien de ce questionnaire doit être accessible en
            permanence et partagé après chaque évènement organisé.}.

            Par ailleurs, \orga s'engage à rappeler dans sa communication que
            la protection contre les \VSS est l'une des priorités de
            l'évènement.

        \section{Responsables anti-\VSS}

            \orga doit s'assurer du respect des points suivants :
            \begin{itemize}
                \item au moins 20 \% des membre de \orga présentes à
                      l'évènement doivent avoir suivi une formation à la
                      protection contre les \VSS ;
                \item la présence de responsables anti-\VSS doit représenter
                      au moins  2 \% des personnes présentes à la soirée, avec
                      un minimum de  0,5 \% en permanence. Pour un évènement
                      festif autre qu'une soirée, il doit s'y trouver un
                      minimum de 2 responsables anti-\VSS ;
                \item les responsables anti-\VSS ne doivent pas payer leur
                      participation à l'évènement. En effet, leur présence est
                      essentielle et permet de garantir une sécurité accrue.
                      Ils doivent être considérés comme un élargissement du
                      personnel encadrant.
            \end{itemize}

            Il est également nécessaire de souligner que la responsabilité de
            traiter les \VSS représente une charge mentale importante ; un
            roulement régulier doit donc avoir lieu entre les différents
            responsables anti-\VSS.

            Pour cela, \orga peut faire appel à des étudiants et étudiantes
            volontaires ayant suivi une formation pouvant se dérouler au sein
            de leur école respective ou sur le plateau plus généralement.
            \orga s'engage ainsi, si nécessaire, à autoriser des personnes
            externes à l'école à être responsables anti-\VSS pour l'évènement.

            Par ailleurs, les responsables anti-\VSS doivent respecter les
            points suivants :
            \begin{itemize}
                \item avoir été formés pour ce rôle ;
                \item ne pas consommer d'alcool ou de stupéfiants ;
                \item ne pas commettre d'aggression sexuelle ou de harcèlement
                      sexuel.
            \end{itemize}

            \orga doit s'assurer que les responsables anti-\VSS puissent être
            joignables sur une ligne téléphonique neutre, dont le numéro est
            accessible à tout moment par toutes les personnes présentes à
            l'évènement. Ce dernier doit permettre de communiquer facilement,
            discrètement et à tout moment avec les responsables anti-\VSS.
            Toute personne participant peut ainsi faire part d'une situation
            dont elle est acteur ou dont elle est témoin.

        \section{Sécurité et bien-être}

            \orga doit s'assurer de la présence d'une \textit{safe-zone} au
            lieu de l'évènement. Celle-ci doit être une pièce calme, isolée et
            avec la présence de responsables anti-\VSS.

            La \textit{safe-zone} et sa localisation doivent être connues par
            toutes les personnes participant à l'évènement, notamment par la
            communication de \orga, comprenant entre autres des affiches
            disposées dans la salle ou les salles de l'évènement. Celles-ci
            doivent également contenir les numéros d'écoute d'urgence ainsi que
            celui du téléphone anti-\VSS. Ces affiches doivent être communes à
            toutes les écoles du plateau afin de favoriser la cohérence dans la
            communication.

        \section{Communication et responsabilisation}

            \subsection{Participants et participantes}

                Les affiches et le numéro de téléphone anti-\VSS sont les
                principaux outils de responsabilisation des personnes présentes
                à l'évènement. Une communication en amont de la part \orga à
                propos de ces dispositifs est nécessaire. Ceux-ci doivent ainsi
                être présentés dans les mails de communication de l'évènement,
                ainsi que sur les réseaux sociaux.

            \subsubsection{Intervenants et intervenantes externes et personnel}

                Lors d'un évènement nécessitant la présence de vigiles, ceux-ci
                sont garants de la sécurité de tous et toutes. Cela doit par
                ailleurs être assuré par \orga. Pour cela, il est impératif de
                s'informer quant aux éventuelles actions inacceptables de la
                part des vigiles, et de communiquer l'information aux autres
                écoles\footnote{Cela pourra être fait par une liste
                inter-écoles des entreprises de sécurité ainsi que des vigiles
                afin de partager les expériences des associations. Celle-ci
                sera mise en place et complétée à chaque évènement si besoin
                est.}.

                \orga doit assurer la tenue d'une discussion entre les
                responsables anti-\VSS et les vigiles, pour que les deux
                partis se reconnaissent et puissent travailler ensemble lors
                de l'évènement.

    \chapter{Intégration}

        Les associations participant à l'organisation de l'intégration de
        nouveaux entrants doivent s'assurer que celle-ci ne contient aucun jeu
        ni aucun chant participant à la culture du viol, ou impliquant de la
        nudité ou des rapports sexuels sans expression de consentement.

    \chapter{Organisation de l'association}

        Toute association signant cette charte s'engage à :
        \begin{itemize}
            \item condamner les violences sexuelles et sexistes ;
            \item intégrer la charte dans des statuts et/ou règlement
                  intérieur, et la transmettre à ses prochains membres lors de
                  la passation ;
            \item prendre des mesures adéquates, et se réserver le droit
                  d'exclure ses membres qui entravent le bon déroulement des
                  mesures de protection contre les \VSS ;
            \item ne pas favoriser l'action ou l'évènement d'une association
                  qui n'assurerait pas de protection contre les \VSS, ou dont
                  les membres en commettraient.
        \end{itemize}

        \makeend

        \section*{Signature}

            %Je soussigné \rule[-1.5pt]{150pt}{1pt} m'engage à respecter la
            %présente charte.
            Je soussigné \rule[-1.5pt]{100pt}{1pt}, représentant l'association
            \rule[-1.5pt]{110pt}{1pt}, m'engage à respecter les mesures
            ci-dessus, à intégrer la présente charte dans les statuts et/ou
            règlement intérieur de ladite association, et à transmettre ces
            engagements lors de la passation.

            %Nom, prénom et signature, précédé de la mention
            %\gui{Lu et approuvé} :
            Nom de l'association et signature, précédé de la mention
            \gui{Lu et approuvé} :

\end{document}
